import urllib2

def get_html(url):
    return urllib2.urlopen(url).read()

def exchange_rate(html, currency) :
    start = html.find(currency + " / CHF")
    
    if start == -1:
        raise ValueError()
        
    end = html.find("</div>", start)
    html = html[start:end].strip()
    start = html.find('e">')
    end = html.find("</span>", start)
    html = html[start+3:end]
    return html

url = "http://www.snb.ch/fr/"
html = get_html(url)
print exchange_rate(html, "JPY")
    
