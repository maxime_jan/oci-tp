from math import *
import copy

class Point(object):
    "Encore une nouvelle classe de points"
    numberOfPoints = 0

    def __init__(self, coord_x = 0, coord_y = 0):

        Point.numberOfPoints += 1
        self.x = coord_x
        self.y = coord_y

    def __str__(self):
        return "({}; {})".format(self.x, self.y)
    
    def __repr__(self):
        return "Point({}, {})".format(self.x, self.y)

    def __add__(self, other):
        x_res = self.x + other.x
        y_res = self.y + other.y
        res = Point(x_res, y_res)
        return res
    
    def distance(self, other):
        return sqrt((self.x - other.x)**2 + (self.y - other.y)**2)

    @staticmethod
    def count():
        return Point.numberOfPoints

class Rectangle(object):

    def __init__(self, corner, length, height):
        self.corner = corner
        self.length = length
        self.height = height

    def area(self):
        return self.length * self.height
    
    def get_center(self):
        return Point(int(corner.x + self.length / 2), int(corner.y - self.height / 2))

    def clone(self):
        return copy.deepcopy(self)
