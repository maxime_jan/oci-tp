import main.py

#Problème 4
def histogramme_freq(sentence):

    words_list = to_words(sentence)
    words_list_unique = make_unique(words_list)
    length_of_dico = len(words_list_unique)
    words_dico = {}
    
    for word in words_list_unique:
        words_dico[word] = 0
    
    for word in words_list:
        words_dico[word] += 1
    
    words_list_of_tuple = sorted(words_dico.items(), key=lambda x: x[1])
    words_list_of_tuple = words_list_of_tuple[::-1]

    print("Mots\t\tFrequences\tHistogramme\n===================================================\n")
    for word, number in words_list_of_tuple:
        print(word+"\t\t"+str(number)+"\t\t"+"*" * number)

# def histogramme_freq_GPanel(sentence):

#     words_list = to_words(sentence)
#     words_list_unique = make_unique(words_list)
#     length_of_dico = len(words_list_unique)
#     words_dico = {}
    
#     for word in words_list_unique:
#         words_dico[word] = 0
    
#     for word in words_list:
#         words_dico[word] += 1
    
#     words_list_of_tuple = sorted(words_dico.items(), key=lambda x: x[1])
#     words_list_of_tuple = words_list_of_tuple[::-1]

#     histo = [value[1] for value in words_list_of_tuple]

#     makeGPanel(-1, len(histo), -1, max(histo)+1)
#     drawGrid(0, len(histo)-1, 0, max(histo), 1, max(histo))
#     lineWidth(5)
#     for n in range(len(histo)):
#         setColor("red")
#         line(n, 0, n, histo[n])
#         setColor("black")
#         text(n,-0.25, words_list_of_tuple[n][0])
    
# histogramme_freq_GPanel("le corbeau et le renard")