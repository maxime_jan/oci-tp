

#Problème 1a
def to_words(text):
    punctuation = ',;.:?!"\''
    words = []
    words = "".join(letter for letter in text if letter not in punctuation).split(" ")
    return words

#Problème 1b
def max_word_len(sentence):
    words = to_words(sentence)
    word_count = (0,0)
    for i in range(len(words)):
        if len(words[i]) > word_count[1]:
            word_count = (i+1, len(words[i])) #Ajout de 1 à l'index, car par exemple si le plus long mot est le premier, on veut qu'il soit 
            #en position 1 et non 0
    return word_count

#Problème 2
def make_unique(elements):
    unique_elements = []
    [unique_elements.append(element) for element in elements if not unique_elements.count(element)]
    return unique_elements

#Problème 3
def swap_key_values(dico):
    new_dico = {}
    for (key, value) in dico.items():
        new_dico[value] = key
    return new_dico
