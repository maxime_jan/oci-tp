from time import time

#Problème 1
def timeit(function):
        t0 = time()
        function()
        t1 = time()
        elapsed_time = t1-t0
        return elapsed_time

#Problème 2
def timeit_n(function, n):
        total_time = 0
        for _ in range(n):
            t0 = time()
            function()
            t1 = time()
            total_time += (t1-t0)
        return total_time/n

#Problème 4
def fast_power(a, n):
    if n == 0:
        return 1
        
    if n < 0:
        if n % 2 == 1:
            return 1 / power(a, -n)
        else:
            return 1 / (power(a, -n / 2) * power(a, -n / 2))
    else:
        if n % 2 == 1:
            return a * power(a, n - 1)
        
        else:
            return power(a, n / 2) * power(a, n / 2)